import binascii
import os
from email.utils import parseaddr

from shakestir_tests.model.user import User


def test_login_and_search_unique_recipe(app):
    app.ensure_logout()
    app.login(User.valid_user())
    assert app.is_logged_in()

    assert app.search_recipes('Iollina Collada') == 1


def test_is_user_email_valid(app):
    app.ensure_logout()
    app.login(User.valid_user())
    assert app.is_logged_in()

    assert '@' in parseaddr(app.get_user_email())[1]


def test_login_and_add_recipe(app):
    app.ensure_logout()
    app.login(User.valid_user())
    assert app.is_logged_in()

    app.open_add_recipe()

    cocktail_name = 'The Pios ' + get_random_string()
    image_path = os.path.abspath('shakestir_tests\\resources\\pios.jpg')
    description = 'The noble cocktail Pios'
    method = 'Take all the ingredients and mix'
    inspiration = 'The noble dog Pios'
    app.fill_new_recipes_fields(cocktail_name, image_path, inspiration=inspiration, description=description,
                                method=method)

    ingredient1 = 'Wine ' + get_random_string()
    ingredient2 = 'Oil ' + get_random_string()
    measure = 'teaspoon'

    app.add_new_ingredient(ingredient1, measure)
    app.add_new_ingredient(ingredient2, measure)

    app.add_recipe()

    app.click_on_first_recipe()

    recipe_page = app.get_recipe_info()

    assert cocktail_name in recipe_page.cocktail_name
    assert description in recipe_page.description
    assert inspiration in recipe_page.inspiration
    assert ingredient1 in recipe_page.ingredient1
    assert ingredient2 in recipe_page.ingredient2


def get_random_string():
    return binascii.b2a_hex(os.urandom(4))
