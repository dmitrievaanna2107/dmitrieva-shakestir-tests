from shakestir_tests.pages.base_page import BasePage
from shakestir_tests.pages.header_page import HeaderPage


class AddNewIngredient(HeaderPage):

    def __init__(self, driver):
        BasePage.__init__(self, driver)
        self.ingredient_field = self.find_element_by_id('dnn_ctr581_RecipeEditor_txtIngredientText')
        self.amount_field = self.find_element_by_id('dnn_ctr581_RecipeEditor_txtAmount')

        self.save_ingredient_button = self.find_element_by_id('dnn_ctr581_RecipeEditor_btnSaveIngredient')

    def set_ingredient_text(self, text):
        BasePage.fill_text_field(self.ingredient_field, text)

    def set_amount_text(self, text):
        BasePage.fill_text_field(self.amount_field, text)

    def select_measure_element(self):
        self.list_element_selection(".//*[@id='dnn_ctr581_RecipeEditor_ddlMeasurement']")

    def save_ingredient_button_click(self):
        BasePage.button_click(self.save_ingredient_button)
