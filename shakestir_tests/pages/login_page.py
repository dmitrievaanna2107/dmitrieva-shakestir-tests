from shakestir_tests.pages.base_page import BasePage
from shakestir_tests.pages.header_page import HeaderPage


class LoginPage(HeaderPage):

    def __init__(self, driver):
        HeaderPage.__init__(self, driver)
        self.username_field = self.find_element_by_id('dnn_ctr413_Loader_ctl00_txtStandardUsername')
        self.password_field = self.find_element_by_id('dnn_ctr413_Loader_ctl00_txtStandardPassword')
        self.submit_button = self.find_element_by_id('dnn_ctr413_Loader_ctl00_btnStandardLogin')

    def set_username_text(self, text):
        BasePage.fill_text_field(self.username_field, text)

    def set_password_text(self, text):
        BasePage.fill_text_field(self.password_field, text)

    def submit_button_click(self):
        BasePage.button_click(self.submit_button)
