import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import *
from selenium.webdriver.support.select import Select


class BasePage:
    """
    This is common description of a page on the site.
    """

    def __init__(self, driver):
        self.driver = driver
        self.web_driver_wait = WebDriverWait(driver, 10)

    # for assertions. Returns true/false
    def is_element_visible(self, locator):
        try:
            return self.web_driver_wait.until(visibility_of_element_located(locator))
        except WebDriverException:
            return False

    def is_titles_equal(self, title):
        return self.driver.Title == title

    # for finding element
    def find_element_by_xpath(self, locator):
        return self.driver.find_element(By.XPATH, locator)

    def find_element_by_css(self, locator):
        return self.driver.find_element(By.CSS_SELECTOR, locator)

    def find_element_by_link_text(self, locator):
        return self.driver.find_element(By.LINK_TEXT, locator)

    def find_element_by_id(self, locator):
        return self.driver.find_element(By.ID, locator)

    # for finding elements

    def find_elements_by_xpath(self, locator):
        return self.driver.find_elements(By.XPATH, locator)

    def find_elements_by_css(self, locator):
        return self.driver.find_elements(By.CSS_SELECTOR, locator)

    def find_elements_by_link_text(self, locator):
        return self.driver.find_elements(By.LINK_TEXT, locator)

    def find_elements_by_id(self, locator):
        return self.driver.find_elements(By.ID, locator)

    # for clicking on element
    def click_on_element_by_xpath(self, locator):
        return self.driver.find_element(By.XPATH, locator).click()

    def click_on_element_by_link_text(self, locator):
        return self.driver.find_element(By.LINK_TEXT, locator).click()

    # for working with drop-down elements in menu
    def menu_element_selection(self, element):
        actions = ActionChains(self.driver)
        actions.move_to_element(element)
        actions.perform()

    def menu_element_click(self, element):
        actions = ActionChains(self.driver)
        actions.move_to_element(element).click()
        actions.perform()

    # for working with drop-down lists
    def list_element_selection(self, locator):
        select = Select(self.find_element_by_xpath(locator))
        select.select_by_index(2)

    @staticmethod
    def wait():
        time.sleep(5)

    @staticmethod
    def button_click(button):
        button.click()
        BasePage.wait()

    @staticmethod
    def fill_text_field(text_field, text):
        text_field.clear()
        text_field.send_keys(text)

    @staticmethod
    def fill_form(form, text):
        form.send_keys(text)
        BasePage.wait()
