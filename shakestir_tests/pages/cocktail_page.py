from shakestir_tests.pages.base_page import BasePage
from shakestir_tests.pages.header_page import HeaderPage


class CocktailPage(HeaderPage):
    def __init__(self, driver):
        BasePage.__init__(self, driver)
        self.cocktail_name = self.find_element_by_xpath(".//*[@id='dnn_ctr581_ModuleContent']/div[1]/div[1]/div[2]/h1").text
        self.description = self.find_element_by_xpath(
            ".//*[@id='dnn_ctr581_ModuleContent']/div[1]/div[1]/div[2]/p[2]").text
        self.inspiration = self.find_element_by_xpath(".//*[@id='dnn_ctr581_ModuleContent']/div[1]/div[2]/div[2]").text
        self.ingredient1 = self.find_element_by_xpath(
            ".//*[@id='dnn_ctr581_ModuleContent']/div[1]/div[2]/div[6]/div[1]/ul/span[1]/li/span[2]").text
        self.ingredient2 = self.find_element_by_xpath(
            ".//*[@id='dnn_ctr581_ModuleContent']/div[1]/div[2]/div[6]/div[1]/ul/span[2]/li/span[2]").text
