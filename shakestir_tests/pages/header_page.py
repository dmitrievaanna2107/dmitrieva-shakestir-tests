from selenium.webdriver.common.by import By

from shakestir_tests.pages.base_page import BasePage


class HeaderPage(BasePage):
    """
    Selectors on internal page
    """

    def __init__(self, driver):
        BasePage.__init__(self, driver)

    def get_login_button(self):
        return self.find_element_by_xpath(".//*[@href='/login']")

    def get_profile_popup(self):
        return self.find_element_by_xpath(".//*[@href='/profile']")

    def get_logout_button(self):
        return self.find_element_by_xpath(".//*[@id='dnn_dnnLogin_cmdLogin']")

    def login_button_click(self):
        BasePage.button_click(self.get_login_button())

    def perform_logout(self):
        self.menu_element_selection(self.get_profile_popup())
        BasePage.wait()
        self.menu_element_click(self.get_logout_button())
        BasePage.wait()

    def get_user_email(self):
        self.button_click(self.get_profile_popup())
        self.button_click(self.find_element_by_id('dnn_ctr446_View_ViewProfile_lnkShowContactInfo'))
        return self.find_element_by_xpath(".//*[@id='EmailInformation']/ul/li").text

    def recipe_button_click(self):
        BasePage.button_click(self.find_element_by_xpath(".//*[@id='dnn_dnnRADMENUACTIONS_RadMenu1']/ul/li[6]/a/span"))

    def is_logged_in(self):
        return self.is_element_visible((By.XPATH, ".//*[@href='/profile']"))
