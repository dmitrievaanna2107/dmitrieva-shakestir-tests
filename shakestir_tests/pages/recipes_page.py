from selenium.webdriver.common.keys import Keys

from shakestir_tests.pages.base_page import BasePage
from shakestir_tests.pages.header_page import HeaderPage


class RecipesPage(HeaderPage):

    def __init__(self, driver):
        BasePage.__init__(self, driver)

    def search_recipe(self, text):
        search_field = self.find_element_by_id('dnn_ctr582_View_txtRecipeSearch')
        BasePage.fill_text_field(search_field, text)
        search_field.send_keys(Keys.ENTER)
        BasePage.wait()

    def get_recipes_count(self):
        return len(self.find_elements_by_id('recipes-list'))

    def add_recipe_button_click(self):
        BasePage.button_click(self.find_element_by_xpath(".//*[@id='options']/a[2]/span"))

    def first_recipe_click(self):
        BasePage.button_click(self.find_element_by_xpath(".//*[@id='recipes-list']/div[1]/a[2]"))
