from shakestir_tests.pages.base_page import BasePage
from shakestir_tests.pages.header_page import HeaderPage


class AddRecipePage(HeaderPage):
    def __init__(self, driver):
        BasePage.__init__(self, driver)
        self.title_field = self.find_element_by_id('dnn_ctr581_RecipeEditor_txtTitle')
        self.description_field = self.find_element_by_id('dnn_ctr581_RecipeEditor_txtLongDescription')
        self.method_field = self.find_element_by_id('dnn_ctr581_RecipeEditor_txtInstructions')
        self.servings_field = self.find_element_by_id('dnn_ctr581_RecipeEditor_txtServings')
        self.inspiration_field = self.find_element_by_id('dnn_ctr581_RecipeEditor_txtInspiration')

        self.add_new_ingredient_button = self.find_element_by_xpath(
            ".//*[@id='dnn_ctr581_RecipeEditor_pnlRecipeLeft']/div[1]/div[4]/div/div/div[1]/div[2]/div/a/span")

        self.submit_button = self.find_element_by_id('dnn_ctr581_RecipeEditor_btnSave')

        self.category_button1 = self.find_element_by_id('dnn_ctr581_RecipeEditor_cblCategories_2')

        self.category_button2 = self.find_element_by_id('dnn_ctr581_RecipeEditor_cblCategories_9')

    def set_title_text(self, text):
        BasePage.fill_text_field(self.title_field, text)

    def tick_category_checkboxes(self):
        self.category_button1.click()
        self.category_button2.click()

    def set_description_text(self, text):
        BasePage.fill_text_field(self.description_field, text)

    def set_method_text(self, text):
        BasePage.fill_text_field(self.method_field, text)

    def set_servings_text(self, text):
        BasePage.fill_text_field(self.servings_field, text)

    def set_inspiration_text(self, text):
        BasePage.fill_text_field(self.inspiration_field, text)

    def add_new_ingredient_button_click(self):
        BasePage.button_click(self.add_new_ingredient_button)

    def submit_button_click(self):
        BasePage.button_click(self.submit_button)
        BasePage.wait()

    def add_photo(self, image_path):
        BasePage.button_click(self.find_element_by_id('btnImageUploadModal'))
        self.driver.switch_to_frame(self.find_element_by_id('mainImgUploadIframe'))
        BasePage.button_click(self.find_element_by_xpath(".//*[@id='pnlHTML5Uploader']/div/a"))
        BasePage.fill_form(self.find_element_by_id('dnn_ctr579_View_fuImage'), image_path)
        BasePage.button_click(self.find_element_by_id('dnn_ctr579_View_btnUploadImage'))
        BasePage.wait()
        self.driver.switch_to_default_content()
