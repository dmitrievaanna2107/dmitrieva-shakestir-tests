from shakestir_tests.pages.add_cocktail_page import AddRecipePage
from shakestir_tests.pages.add_ingredient_page import AddNewIngredient
from shakestir_tests.pages.cocktail_page import CocktailPage
from shakestir_tests.pages.header_page import HeaderPage
from shakestir_tests.pages.base_page import BasePage
from shakestir_tests.pages.login_page import LoginPage
from shakestir_tests.pages.recipes_page import RecipesPage


class Application(object):
    """
    This class describes the logic of application.
    """

    def __init__(self, driver, base_url):
        self.driver = driver
        driver.get(base_url)
        BasePage.wait()
        self.current_page = HeaderPage(driver)

    # User authorization
    def login(self, user):
        self.current_page.login_button_click()
        self.current_page = LoginPage(self.driver)
        self.current_page.set_username_text(user.username)
        self.current_page.set_password_text(user.password)
        self.current_page.submit_button_click()

    def logout(self):
        self.current_page.perform_logout()

    def open_add_recipe(self):
        self.open_recipes()
        self.current_page = RecipesPage(self.driver)
        self.current_page.add_recipe_button_click()

    def search_recipes(self, text):
        self.open_recipes()
        self.current_page = RecipesPage(self.driver)
        self.current_page.search_recipe(text)
        return self.current_page.get_recipes_count()

    def open_recipes(self):
        self.current_page = HeaderPage(self.driver)
        self.current_page.recipe_button_click()

    def get_user_email(self):
        self.current_page = HeaderPage(self.driver)
        return self.current_page.get_user_email()

    def fill_new_recipes_fields(self, cocktail_name, photo_path, description='',
                                method='', inspiration='',
                                servings='1'):
        self.current_page = AddRecipePage(self.driver)

        self.current_page.set_title_text(cocktail_name)
        self.current_page.set_description_text(description)
        self.current_page.set_method_text(method)
        self.current_page.set_inspiration_text(inspiration)
        self.current_page.set_servings_text(servings)
        self.current_page.tick_category_checkboxes()

        self.current_page.add_photo(photo_path)

    def add_new_ingredient(self, ingredient_field, measure_element, amount='1'):

        self.current_page = AddRecipePage(self.driver)
        self.current_page.add_new_ingredient_button_click()

        self.current_page = AddNewIngredient(self.driver)

        self.current_page.set_ingredient_text(ingredient_field)
        self.current_page.set_amount_text(amount)
        self.current_page.select_measure_element()

        self.current_page.save_ingredient_button_click()

    def add_recipe(self):
        self.current_page = AddRecipePage(self.driver)
        self.current_page.submit_button_click()

    def click_on_first_recipe(self):
        self.current_page = RecipesPage(self.driver)
        self.current_page.first_recipe_click()

    def get_recipe_info(self):
        self.current_page = CocktailPage(self.driver)
        return self.current_page

    def is_logged_in(self):
        return self.current_page.is_logged_in()

    def is_not_logged_in(self):
        return not self.current_page.is_logged_in()

    def ensure_logout(self):
        if self.is_logged_in():
            self.logout()
