from random import randint


class User(object):
    """
    This class contains user's parameters.
    """
    def __init__(self, username="", password="", email=""):
        self.username = username
        self.password = password
        self.email = email

    # TODO Insert your credentials
    @classmethod
    def valid_user(cls):
        return cls(username='annatest', password='1234567')

    @classmethod
    def random(cls):
        return cls(username='user%s' % randint(1, 1000),
                   password='password%s' % randint(1, 1000),
                   email='user%s@test.com' % randint(1, 1000))
