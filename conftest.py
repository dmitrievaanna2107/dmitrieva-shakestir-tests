import pytest
from selenium import webdriver

from shakestir_tests.model.application import Application


def pytest_addoption(parser):
    """
    Adds parameters at moment of pytest starting.
    """
    parser.addoption("--br", action="store", default="firefox", help="browser type")
    parser.addoption("--url", action="store", default="http://shakestir-stg-2.dev.thumbtack.net", help="base URL")



@pytest.fixture(scope="session")
def browser_type(request):
    return request.config.getoption("--br")


@pytest.fixture(scope="session")
def base_url(request):
    return request.config.getoption("--url")


@pytest.fixture(scope="session")
def app(request, browser_type, base_url):
    if browser_type == 'firefox':
        driver = webdriver.Firefox()
    elif browser_type == 'chrome':
        driver = webdriver.Chrome()
    elif browser_type == 'ie':
        driver = webdriver.Ie()
    request.addfinalizer(driver.quit)
    driver.maximize_window()
    return Application(driver, base_url)


